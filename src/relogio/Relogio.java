package relogio;

public class Relogio {

	private Horario hms;
	private Data dma;
	
	public Relogio(Horario hms, Data dma) {
		this.hms = new Horario(hms);
		this.dma = dma;
	}
	
	public void tictac() {
		
		hms.incrementaSegundo();

		if(hms.ehPrimeiroHorario()) {
			dma.incrementaDia();
		}		
	}
	
	@Override
	public String toString() {
		return dma + " " + hms;
	}
}

